package com.fetch.points.repository;

import com.fetch.points.model.domain.Payer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PayerRepository extends CrudRepository<Payer, UUID> {
}
