package com.fetch.points.repository;

import com.fetch.points.model.domain.Balance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
import java.util.List;

@Repository
public interface BalanceRepository extends CrudRepository<Balance, UUID> {
    Balance findByCustomerIdAndPayerId(UUID customerID, UUID payerID);
    List<Balance> findByCustomerId(UUID customerID);
}
