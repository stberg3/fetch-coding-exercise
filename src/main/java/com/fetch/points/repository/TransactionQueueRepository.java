package com.fetch.points.repository;


import com.fetch.points.model.domain.Transaction;
import org.springframework.stereotype.Repository;

import java.util.UUID;

import javax.persistence.Query;

import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;
import java.util.Queue;
import java.util.PriorityQueue;

@Repository
public class TransactionQueueRepository {
    
    private Map<UUID, Queue<Transaction>> userTransactions;

    public TransactionQueueRepository() {
        userTransactions = new HashMap<>();
    }

    public Transaction addTransaction(Transaction transaction) {
        UUID customerID = transaction.getCustomer().getId();
        
        if(userTransactions.containsKey(customerID)) {
            userTransactions.get(customerID).add(transaction);
        } else {
            userTransactions.put(customerID, new PriorityQueue<>());
        }

        return transaction;
    }

    public Queue<Transaction> getTransactions(UUID userID) {
        return userTransactions.getOrDefault(userID, new PriorityQueue<>());
    }
}
