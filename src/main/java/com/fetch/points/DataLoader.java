package com.fetch.points;

import com.fetch.points.model.domain.Payer;
import com.fetch.points.repository.PayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements ApplicationRunner {

    private PayerRepository payerRepository;

    @Autowired
    public DataLoader(PayerRepository userRepository) {
        this.payerRepository = userRepository;
    }

    public void run(ApplicationArguments args) {
        Payer account1 = new Payer("user1", 1000);
        Payer account2 = new Payer("user2", 20000);
        Payer account3 = new Payer("user3", 300000);

        payerRepository.save(account1);
        payerRepository.save(account2);
        payerRepository.save(account3);        

    }
}