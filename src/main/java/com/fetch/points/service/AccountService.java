package com.fetch.points.service;

import com.fetch.points.model.domain.Customer;
import com.fetch.points.model.domain.Payer;
import com.fetch.points.repository.PayerRepository;
import com.fetch.points.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AccountService {
    private PayerRepository payerRepository;
    private CustomerRepository customerRepository;

    @Autowired
    public AccountService(PayerRepository payerRepository, 
            CustomerRepository customerRepository) {

        this.payerRepository = payerRepository;
        this.customerRepository = customerRepository;
    }

    public Payer createPayer(String username, Integer balance) {
        Payer payer = new Payer(username, balance);
        return payerRepository.save(payer);
    }

    public List<Payer> getPayers() {
        List<Payer> payers = new ArrayList<>();
        for(Payer payer :  payerRepository.findAll()) {
            payers.add(payer);
        }

        return payers;
    }

    public Payer getPayer(UUID id) {
        Optional<Payer> payer = payerRepository.findById(id);
        if(!payer.isPresent()) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }

        return payer.get();
    }

    public Customer createCustomer() {
        Customer customer = new Customer();
        return customerRepository.save(customer);
    }

    public List<Customer> getCustomers() {
        List<Customer> customers = new ArrayList<>();
        for (Customer customer : customerRepository.findAll()) {
            customers.add(customer);
        }

        return customers;
    }

    public Customer getCustomer(UUID id) {
        Optional<Customer> customer = customerRepository.findById(id);
        if (!customer.isPresent()) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }

        return customer.get();
    }
}
