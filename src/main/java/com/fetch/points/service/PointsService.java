package com.fetch.points.service;

import com.fetch.points.model.domain.Balance;
import com.fetch.points.model.domain.Customer;
import com.fetch.points.model.domain.Transaction;
import com.fetch.points.repository.BalanceRepository;
import com.fetch.points.repository.TransactionRepository;
import com.fetch.points.repository.TransactionQueueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.Queue;

@Service
public class PointsService {
    private BalanceRepository balanceRepository;
    private TransactionRepository transactionRepository;
    private TransactionQueueRepository transactionQueueRepository;

    @Autowired
    public PointsService(
        BalanceRepository balanceRepository,
        TransactionRepository transactionRepository,
        TransactionQueueRepository transactionQueueRepository) {

        this.balanceRepository = balanceRepository;
        this.transactionRepository = transactionRepository;
        this.transactionQueueRepository = transactionQueueRepository;
    }

    @Transactional
    public void addTransaction(Transaction transaction) {
        transactionRepository.save(transaction);

        if(transaction.getAmount() > 0 ){
            transactionQueueRepository.addTransaction(transaction);
        }
        
        Balance originalBalance = balanceRepository.findByCustomerIdAndPayerId(
                                    transaction.getCustomer().getId(), 
                                    transaction.getPayer().getId());

        originalBalance.setBalance(originalBalance.getBalance() + transaction.getAmount());
        balanceRepository.save(originalBalance);
    }

    @Transactional
    public List<Balance> spendPoints(Customer customer, int points) {
        int totalBalance = getPoints(customer.getId());
        
        if (points > totalBalance) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }

        Queue<Transaction> transactions = transactionQueueRepository.getTransactions(customer.getId());
        List<Transaction> newTransactions = new ArrayList<>();

        while(!transactions.isEmpty() && points > 0) {
            Transaction transaction = transactions.peek();
            int newTransactionAmt;
            
            if(points < transaction.getAmount()) {
                newTransactionAmt = -points;
                points = 0;
            } else {
                newTransactionAmt = -transaction.getAmount();
                points -= transaction.getAmount();
                transactions.poll();
            }

            Transaction newTransaction = new Transaction(newTransactionAmt, OffsetDateTime.now());
            newTransaction.setCustomer(transaction.getCustomer());
            newTransaction.setPayer(transaction.getPayer());     
            newTransactions.add(transaction);
            addTransaction(newTransaction);
        }

        return getBalances(customer.getId());
    }

    public int getPoints(UUID customerID) {
        List<Balance> balances = getBalances(customerID);
        int total = 0;

        for(Balance balance : balances) {
            total += balance.getBalance();
        }

        return total;
    }

    public List<Balance> getBalances(UUID customerID) {
        return balanceRepository.findByCustomerId(customerID);
    }

}
