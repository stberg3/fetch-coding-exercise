package com.fetch.points.model.domain;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@RequiredArgsConstructor
public class Payer {
    @Id
    @GeneratedValue
    private UUID id;

    @NonNull
    @Column(unique=true)
    private String username;

    @NonNull
    private Integer balance;

    @OneToMany
    private List<Transaction> transactionList;

}
