package com.fetch.points.model.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@Entity
public class Customer {
    @Id
    @GeneratedValue
    private UUID id;

    @Transient
    private int balance;

    @OneToMany
    private List<Balance> balances;

    @ManyToOne
    private List<Transaction> transactionList;
}
