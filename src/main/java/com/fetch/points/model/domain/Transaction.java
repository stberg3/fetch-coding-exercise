package com.fetch.points.model.domain;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@RequiredArgsConstructor
public class Transaction implements Comparable<Transaction> {
    @Id
    @GeneratedValue
    private UUID id;

    @NonNull
    private Integer amount;

    @NonNull
    private OffsetDateTime date;

    @OneToMany
    private Payer payer;

    @OneToMany
    private Customer customer;

    @Override
    public int compareTo(Transaction other) {
        return other.getDate().compareTo(this.getDate());
    }

}
