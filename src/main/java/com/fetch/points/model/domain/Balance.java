package com.fetch.points.model.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NonNull;

import java.util.UUID;

@Data
@Entity
public class Balance {
    @Id
    @GeneratedValue
    private UUID id;

    @NonNull
    private Integer balance;

    @NonNull
    private Payer payer;

    @NonNull
    private Customer customer;
}
