package com.fetch.points.model.dto;

import lombok.Data;

@Data
public class PayerAccountDTO {
    private String username;
    private Long balance;
}
