package com.fetch.points.model.dto;

import lombok.Data;

import java.util.Date;

@Data
public class TransactionDTO {
    private String payerName;
    private Long transactionAmount;
    private Date timestamp;
}
