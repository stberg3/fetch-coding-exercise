package com.fetch.points.model.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TransactionListDTO {
    private List<TransactionDTO> transactions = new ArrayList<>();
}
